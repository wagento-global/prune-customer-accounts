<?php
/**
 * Wagento Prune Customer
 *
 * Prune customers that have no orders and no addresses OR email address that seems SPAM
 * Via console command
 *
 * @author Rudie Wang <rudi.wang@wagento.com>
 * @copyright Copyright (c) Wagento (https://wagento.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */
declare(strict_types=1);

namespace Wagento\PruneCustomer\Console\Command;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PruneCustomerShow extends Command
{

    const SPAM_EMAILS_ARGUMENT = "spam-emails";

    const OPTION_DELETE = "delete";
    const OPTION_NO_ADDRESS_ORDER = "no-address-order";
    const OPTION_SPAM_IDENTIFIER = "spam-identifier";
    const OPTION_OUT_CSV = "csv";

    const COMMON_SPAM_EMAILS = array(
        '%.ru',
        '%yandex%'
    );
    const COMMON_SPAM_NAMES = array(
        '%http%'
    );

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var State
     */
    protected $state;

    /**
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws LogicException When the command name is empty
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        OrderCollectionFactory $orderCollectionFactory,
        State $state,
        string $name = null
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->state = $state;
        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->state->setAreaCode(Area::AREA_ADMINHTML);

        // Get arguments and options parameters
        $spamEmails = $input->getArgument(self::SPAM_EMAILS_ARGUMENT);
        $isSpamIdentifier = $input->getOption(self::OPTION_SPAM_IDENTIFIER);
        $isNoAddressOrder = $input->getOption(self::OPTION_NO_ADDRESS_ORDER);
        $isCsv = $input->getOption(self::OPTION_OUT_CSV);
        $isDelete = $input->getOption(self::OPTION_DELETE);

        $filterArray = []; // spam email & name filter array
        $idNinArray = []; // spam email & name IDs array
        $deleteConfirm = false;

        // Ask delete confirmation if $isDelete
        if ($isDelete) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('We highly suggest to create database backup before this action. Are you sure with deleting found spam customers? [yes/no]: ', false);
            $deleteConfirm = $helper->ask($input, $output, $question);
        }

        // Show header as CSV view
        $rows = [];
        $headers = ['ID', 'Email', 'Name', 'SpamCase'];
        if ($isCsv) $output->writeln(implode(',', $headers));

        /*
         * STEP 1 : Get spam customers based on common spam emails & names and additional emails from input arguments
         */

        // Search criteria with common spam emails and names
        if ($isSpamIdentifier) {
            foreach (self::COMMON_SPAM_EMAILS as $email) {
                $filterArray[] = array(
                    'attribute' => 'email',
                    'like' => $email
                );
            }
            foreach (self::COMMON_SPAM_NAMES as $name) {
                $filterArray[] = array(
                    'attribute' => 'firstname',
                    'like' => $name
                );
                $filterArray[] = array(
                    'attribute' => 'lastname',
                    'like' => $name
                );
            }
        }
        // Search criteria with additional spam email arguments
        foreach ($spamEmails as $spamEmail) {
            $filterArray[] = array(
                'attribute' => 'email',
                'like' => $spamEmail
            );
        }

        // Get customer collection with above search criteria
        if (count($filterArray)) {
            $spamCustomerCollection = $this->collectionFactory->create();
            $spamCustomers = $spamCustomerCollection->addAttributeToSelect('id, email, name')
                ->addAttributeToFilter($filterArray);

            foreach ($spamCustomers as $customer) {
                $rowData = [
                    'ID' => $customer->getId(),
                    'Email' => $customer->getEmail(),
                    'Name' => strip_tags($customer->getName()),
                    'SpamCase' => 'Spam Name/Email',
                ];

                $rows[] = $rowData;
                $idNinArray[] = $customer->getId();

                if ($isCsv) $output->writeln($customer->getId() . ',"' . $customer->getEmail() . '","' . $customer->getName() . '","Spam Name/Email"'); // Show result as CSV view

                // Delete if needed
                if ($deleteConfirm == 'yes') {
                    try {
                        // TODO: Create a log if you'll like to a record of deleted customers.
                        $customer->delete();
                        // break;  // Uncomment this to delete one at a time for testing.
                    } catch (\Exception $e) {
                        // TODO: Handle exception. Just printing for now.
                        $output->writeln("Error occurred while trying to delete SPAM customer: " . $e->getMessage());
                        exit;
                    }
                }
            }
        }

        /*
         * STEP 2 : Get SPAM customers based on no address and no order
         */

        // Get customer collection with no address and no order
        if ($isNoAddressOrder) {
            $customerCollection = $this->collectionFactory->create();
            $customers = $customerCollection->addAttributeToSelect('id, email, name');

            // Excluded already listed customers
            if (count($idNinArray)) $customers->addAttributeToFilter('id', array('nin' => $idNinArray));

            foreach ($customers as $customer) {
                $addresses = $customer->getAddresses();
                // if address exist, continue
                if ($addresses) {
                    continue;
                }
                // if address isn't exist, check order count
                else {
                    $orders = $this->orderCollectionFactory->create();
                    $orders = $orders->addAttributeToSelect('*')
                        ->addAttributeToFilter(
                            'customer_id', $customer->getId()
                        )
                        ->load();

                    // if order count is greater than 0, continue
                    if ($orders->count()) {
                        continue;
                    }
                    // if order count is 0, consider as SPAM
                    else {
                        $rowData = [
                            'ID'        => $customer->getId(),
                            'Email'     => $customer->getEmail(),
                            'Name'      => strip_tags($customer->getName()),
                            'SpamCase'  => 'No Address/Order',
                        ];

                        $rows[] = $rowData;

                        if ($isCsv) $output->writeln($customer->getId() . ',"' . $customer->getEmail() . '","' . $customer->getName() . '","No Address/Order"'); // Show result as CSV view

                        // Delete if needed
                        if ($deleteConfirm == 'yes') {
                            try {
                                // TODO: Create a log if you'll like to a record of deleted customers.
                                $customer->delete();
                                // break;  // Uncomment this to delete one at a time for testing.
                            } catch (\Exception $e) {
                                // TODO: Handle exception. Just printing for now.
                                $output->writeln("Error occurred while trying to delete SPAM customer: " . $e->getMessage());
                                exit;
                            }
                        }
                    }
                }
            }
        }

        // Show result as table view
        if (!$isCsv) {
            $table = new Table($output);
            $table->setHeaders($headers);
            $table->addRows($rows);
            $table->render();

            $cnt = count($rows);
            $output->writeln("We've found totally $cnt customers that seems SPAM!!!");
        }
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName("prune_customer:list");
        $this->setDescription("Show SPAM customer lists to prune");
        $this->setDefinition([
            new InputArgument(self::SPAM_EMAILS_ARGUMENT, InputArgument::IS_ARRAY, "Search criteria with SPAM email"),
            new InputOption(self::OPTION_NO_ADDRESS_ORDER, "-a", InputOption::VALUE_NONE, "Search criteria with no address AND no order"),
            new InputOption(self::OPTION_SPAM_IDENTIFIER, "-s", InputOption::VALUE_NONE, "Search criteria with commonly known SPAM cases"),
            new InputOption(self::OPTION_OUT_CSV, null, InputOption::VALUE_NONE, "Show result as comma separated instead of table structure"),
            new InputOption(self::OPTION_DELETE, null, InputOption::VALUE_NONE, "Delete customers with Search criteria"),
        ]);
        parent::configure();
    }
}

