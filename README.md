# Wagento Prune Customer

- [Main Functionalities](#markdown-header-main-functionalities)
- [Requirements](#markdown-header-requirements)
- [Installation](#markdown-header-installation)
- [Updating](#markdown-header-updating)
- [Uninstalling](#markdown-header-uninstalling)
- [CLI Usage](#markdown-header-cli-usage)


## Main Functionalities
Wagento Prune Customer extension add CLI commands that

* finds SPAM customers such as no address or no order or ".ru" emails,
* lists SPAM customers as comma separated CSV format or table structure,
* prunes SPAM customers.

## CLI Usage

* to list only SPAM customers that are commonly known (includes "%.ru", "%yandex%" or "http" in email or name), use option `-s` or `--spam-identifier`:
>$ php bin/magento prune_customer:list --spam-identifier

* to list only SPAM customers with no address and no orders, use option `-a` or `--no-address-order`:
>$ php bin/magento prune_customer:list --no-address-order

* to list both SPAM customers that are commonly known and with no address and no orders customers, use option `-s` or `--spam-identifier` and `-a` or `--no-address-order`:
>$ php bin/magento prune_customer:list --spam-identifier --no-address-order

* to list SPAM customers with custom email search criteria, use argument `spam-emails`:
>$ php bin/magento prune_customer:list "%.ru" "%yandex%"

* to list SPAM customers as comma separated CSV format, use option `--csv`. If no `--csv` option used, it will show result as table structure:
>$ php bin/magento prune_customer:list --csv

* you can pipe CSV result into any file:
>$ php bin/magento prune_customer:list --csv >> spam_list_001.csv

* to list and delete SPAM customers, use option `--delete`:
>$ php bin/magento prune_customer:list --delete
>
>We highly suggest to create database backup before this action. Are you sure with deleting found SPAM customers? [yes/no]:

**Note**: This will ask confirmation question and type `yes` to proceed, type nothing or anything to skip delete process.


* to check help document for CLI command, use option `-h` or `--help`:
>$ php bin/magento prune_customer:list --help

```
Description:
  Show SPAM customer lists to prune

Usage:
  prune_customer:list [options] [--] [<spam-emails>...]

Arguments:
  spam-emails             Search criteria with SPAM email

Options:
  -a, --no-address-order  Search criteria with no address AND no order
  -s, --spam-identifier   Search criteria with commonly known SPAM cases
      --csv               Show result as comma separated instead of table structure
      --delete            Delete customers with Search criteria
  -h, --help              Display this help message
  -q, --quiet             Do not output any message
  -V, --version           Display this application version
      --ansi              Force ANSI output
      --no-ansi           Disable ANSI output
  -n, --no-interaction    Do not ask any interactive question
  -v|vv|vvv, --verbose    Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```


* When you see memory limit issue, you can increase memory limit for CLI command:
>$ php -dmemory_limit=-1 bin/magento prune_customer:list --no-address --no-order


## Requirements

* PHP 7.1.3+, 7.2.0+ or 7.3.0+
* Magento Open Source/Commerce 2.2.0+, 2.3.0+ or 2.4.0+


## Installation

### Composer (recommended)

You can use the following commands to install it from a terminal or command prompt:

    $ cd /path/to/your/site
    $ composer require wagento/module-prune-customer

### Manual

This extension can be downloaded from [GitHub] and installed into the
`app/code` directory of your Magento installation with these commands:

    $ cd /path/to/your/site/app/code
    $ mkdir Wagento
    $ cd Wagento
    $ git clone git@bitbucket.org:wagento-global/prune-customer-accounts.git PruneCustomer

### Post-Install

After installing the extension for the first time, please run these commands to
enable it:

    $ cd /path/to/your/site
    $ php bin/magento module:enable Wagento_PruneCustomer

Once you have enabled the extension, please follow the instructions in the
[Post-Install, Post-Update or Post-Uninstall][post]
section to complete the installation process.

## Updating

### Composer

If you've installed the extension from the Magento Marketplace using Composer,
run these commands from your terminal or command prompt to update it:

    $ cd /path/to/your/site
    $ composer update wagento/module-prune-customer

### Manual

If you've installed the extension from GitHub manually, run these commands from
your terminal or command prompt to update it:

    $ cd /path/to/your/site/app/code/Wagento/PruneCustomer
    $ git pull

## Uninstalling

### Composer

If you've installed the extension from the Magento Marketplace using Composer,
run these commands from your terminal or command prompt to remove its data and
package:

    $ cd /path/to/your/site
    $ php bin/magento module:uninstall -r Wagento_PruneCustomer

### Manual

If you've installed the extension manually, run these commands from your
terminal or command prompt to remove its data:

    $ cd /path/to/your/site/app/code
    $ rm -rf Wagento/PruneCustomer
    $ mysql -u your_user -p your_database <<'SQL'
    DELETE FROM `setup_module` WHERE `module` = 'Wagento_PruneCustomer';
    SQL

## Post-Install, Post-Update or Post-Uninstall

To complete the installation, update or uninstall process, please run these
commands:

    $ cd /path/to/your/site
    $ php bin/magento setup:upgrade
    $ php bin/magento setup:di:compile
    $ php bin/magento setup:static-content:deploy

[post]: #post-install-post-update-or-post-uninstall
