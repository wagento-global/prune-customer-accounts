<?php
/**
 * Wagento Prune Customer
 *
 * Prune customers that have no orders and no addresses OR email address that seems SPAM
 * Via console command
 *
 * @author Rudie Wang <rudi.wang@wagento.com>
 * @copyright Copyright (c) Wagento (https://wagento.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Wagento_PruneCustomer', __DIR__);

